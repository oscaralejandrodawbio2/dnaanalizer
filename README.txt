m15-uf1-pt1-2-b
Project name: DnaAnalizer

Authors:
Alejandro Asensio <alejandro.asensio.10@gmail.com>
Oscar Burgos <mihifidem@gmail.com>

UML diagrams:

// 1
==============================
Class DnaAnalizerApp (main) - by Alejandro
==============================
+ String strandRead
- String[] menuOptions
------------------------------
+ void main
- void run
// Printings:
- int showMenu(
- void showReverseStrand(
- void showComposition() ---> Alejandro
- void showMostRepeated()
- void showLessRepeated() ---> Oscar
------------------------------

// 2
==============================
Class FileHandler - by Oscar
==============================
(no attributes)
------------------------------
+ String readFile(String)
------------------------------

// 3
==============================
Class Analizer - by Alejandro and Oscar
==============================
(no attributes)
------------------------------
// Calculations:
+ String reverseStrand(String) --> Alejandro
+ ArrayList composition(String) --> Alejandro
+ String mostRepeated(String) --> Oscar
+ String lessRepeated(String) --> Oscar
------------------------------


// Expected output:
----
Menu
----
0. Exit
1. Reverse strand
2. Most repeated nucleotide
3. Less repeated nucleotide
4. Nucleotide compotition
> Choose an option [0-4]: 
