/*
Copyright 2018 Oscar Burgos and Alejandro Asensio

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package dnaanalizer;

import java.io.IOException;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class DnaAnalizerApp. Contains the main method that runs the project. IES
 * Provençana. DAWBIO2 M15 UF1 Pt1.2b Git branches
 *
 * @author Oscar Burgos<mihifidem at gmail.com>
 * @author Alejandro Asensio<alejandro.asensio.10 at gmail.com>
 */
public class DnaAnalizerApp {

    String strandRead;

    private final String[] menuOptions = {
        "Exit",
        "Reverse strand",
        "Most repeated nucleotide",
        "Less repeated nucleotide",
        "Nucleotide compotition"
    };

    /**
     * Main function of the application.
     *
     * @param args list of filenames to be processed
     */
    public static void main(String[] args) {
        DnaAnalizerApp myApp = new DnaAnalizerApp();
        myApp.run();
    }

    /**
     * This method runs the code of the application.
     */
    private void run() {
        try {
            strandRead = FileHandler.readFile("src/dnaanalizer/testfiles/test2.txt");
        } catch (IOException ex) {
            Logger.getLogger(DnaAnalizerApp.class.getName()).log(Level.SEVERE, null, ex);
        }
        int optionSelected;
        do {
            /*
            // clear screen (it doesn't work)
            final String ANSI_CLS = "\u001b[2J";
            final String ANSI_HOME = "\u001b[H";
            System.out.print(ANSI_CLS + ANSI_HOME);
            System.out.flush();
             */
            optionSelected = showMenu();
            switch (optionSelected) {
                case 0:
                    System.out.println("Exiting...");
                    break;
                case 1:
                    showReverseStrand();
                    break;
                case 2:
                    showMostRepeated();
                    break;
                case 3:
                    showLessRepeated();
                    break;
                case 4:
                    showComposition();
                    break;
                default:
                    System.out.println("Warning: Choose a valid option.");
            }
        } while (optionSelected != 0);
    }

    /**
     * Shows a menu to the user and asks for an option.
     *
     * @return the selected option or -1 in case of error.
     */
    private int showMenu() {
        int option = -1;
        int numItems = 0;
        System.out.println("----\nMenu\n----");
        for (int i = 0; i < menuOptions.length; i++) {
            System.out.format("%d. %s\n", i, menuOptions[i]);
            numItems++;
        }
        System.out.printf("> Choose an option [0-%d]: ", numItems - 1);
        // Usage of "try-catch" block when a critical error is expected.
        try {
            Scanner sc = new Scanner(System.in);
            option = sc.nextInt();
        } catch (Exception e) {
            System.out.println("Warning: The option must be a number of the menu list.");
        }
        return option;
    }

    /**
     * Prints the reversed strand contained in a given file.
     *
     * @author Alejandro Asensio<alejandro.asensio.10 at gmail.com>
     * @date 2018-10-20
     */
    private void showReverseStrand() {
        String reversedStrand;
        reversedStrand = Analizer.reverseStrand(strandRead);
        System.out.printf("Reversed strand: %s\n", reversedStrand);
    }

    /**
     * Prints the character composition of a string contained in a given file.
     *
     * @author Alejandro Asensio<alejandro.asensio.10 at gmail.com>
     * @date 2018-10-20
     */
    private void showComposition() {
        String result = "";
        Map<String, Integer> comp = Analizer.composition(strandRead);
        String nucleotide;
        Integer count;
        int totalNucleotides = 0;
        for (Map.Entry<String, Integer> entry : comp.entrySet()) {
            nucleotide = entry.getKey();
            count = entry.getValue();
            totalNucleotides += entry.getValue();
            result += nucleotide + ": " + count + "\n";
        }
        System.out.printf("Nucleotide composition:\nTotal nucleotides: %d\n%s", totalNucleotides, result);
    }

    /**
     * Prints the most repeated character of a string, read from a given file.
     *
     * @author Alejandro Asensio<alejandro.asensio.10 at gmail.com>
     * @date 2018-10-22
     */
    private void showMostRepeated() {
        System.out.println(Analizer.mostRepeated(strandRead));
    }

    /**
     * Prints the less repeated character of a string, read from a given file.
     *
     * @author Alejandro Asensio<alejandro.asensio.10 at gmail.com>
     * @date 2018-10-22
     */
    private void showLessRepeated() {
        System.out.println(Analizer.lessRepeated(strandRead));
    }

}
