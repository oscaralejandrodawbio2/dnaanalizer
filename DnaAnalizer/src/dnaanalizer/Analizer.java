/*
Copyright 2018 Oscar Burgos and Alejandro Asensio

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package dnaanalizer;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Alejandro Asensio<alejandro.asensio.10 at gmail.com>
 */
public class Analizer {

    /**
     * Reverses the given strandIn.
     *
     * @param strandIn
     * @return the reversed strandIn
     */
    public static String reverseStrand(String strandIn) {
        String reversedStrand;
        reversedStrand = new StringBuffer(strandIn).reverse().toString();
        return reversedStrand;
    }

    public static Map composition(String strandIn) {
        Map<String, Integer> comp = new HashMap<>();
        String nucleotide;
        int count;
        for (int i = 0; i < strandIn.length(); i++) {
            nucleotide = strandIn.substring(i, i + 1);
            // Ternary conditional
            count = comp.containsKey(nucleotide) ? comp.get(nucleotide) : 0;
            comp.put(nucleotide, count + 1);

        }
        return comp;
    }

    /**
     * function that determines and print from a chain, the most repeated value
     * in the same
     *
     * @param strandIn chain in
     * @return String that contains the less repeated character instrandIn
     */
    public static String mostRepeated(String strandIn) {
        String result;
        int numberMostRepeated = 0;
        int count;
        String letterDna = null;
        HashMap<String, Integer> map = new HashMap<>();
        String[] DnaArray = strandIn.split("");

        for (String Dna : DnaArray) {
            if (map.containsKey(Dna)) {
                count = map.get(Dna);
                map.put(Dna, count + 1);
            } else {
                map.put(Dna, 1);
            }
        }

        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            //System.out.println(entry.getKey() + " : " + entry.getValue());
            if (entry.getValue() >= numberMostRepeated) {
                numberMostRepeated = entry.getValue();
                letterDna = entry.getKey();
            }
        }
        result = "The most repeated nucleotide is " + letterDna + " (" + numberMostRepeated + " repeats).";
        return result;
    }

    /**
     * function that determines and print from a chain, the less repeated value
     * in the same
     *
     * @param strandIn chain in
     * @return String that contains the less repeated character instrandIn
     */
    public static String lessRepeated(String strandIn) {
        String result;
        int numberLessRepeated = Integer.MAX_VALUE;
        int count;
        String letterDna = null;
        HashMap<String, Integer> map = new HashMap<>();
        String[] DnaArray = strandIn.split("");

        for (String Dna : DnaArray) {
            if (map.containsKey(Dna)) {
                count = map.get(Dna);
                map.put(Dna, count + 1);
            } else {
                map.put(Dna, 1);
            }
        }
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            //System.out.println(entry.getKey() + " : " + entry.getValue());
            if (entry.getValue() < numberLessRepeated) {
                numberLessRepeated = entry.getValue();
                letterDna = entry.getKey();
            }
        }
        result = "The less repeated nucleotide is " + letterDna + " (" + numberLessRepeated + " repeats).";
        return result;
    }
}
