/**
 *
 * @author Oscar Burgos<mihifidem at gmail.com>
 * @date 2018-10-21
 */
package dnaanalizer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileHandler {

    /**
     * function that returns the contents of a text file from path sent
     *
     * @param file path+name text file
     * @return String content of the text file
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static String readFile(String file) throws FileNotFoundException, IOException {
        String line;
        String result = "";
        FileReader f = new FileReader(file);
        BufferedReader b = new BufferedReader(f);
        while ((line = b.readLine()) != null) {
            result = result + line;
        }
        b.close();
        return result;
    }

}
